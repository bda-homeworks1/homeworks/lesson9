package HomeWorks.lesson9.task6;

public class Dog extends Animal {
    @Override
    public void eating() {
        System.out.println("dog eating");
    }

    @Override
    public void breath() {
        System.out.println("dog breathe");
    }
}
